/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_Motion_H_
#define __MMM_Motion_H_

#include "../MMMCore.h"
#include "../MMMImportExport.h"
#include "MotionEntries.h"
#include "MotionFrame.h"
#include "AbstractMotion.h"
#include "../Model/Model.h"
#include "../Model/ModelProcessor.h"

#include <Eigen/Core>
#include <string> 
#include <vector>
#include <map>

namespace MMM
{

/**
	@brief A Motion defines a series of MotionFrame snapshots.
	The data covers position, velocity and acceleration of the models root position and orientation 
	and a set of joint values, velocities and accelerations.
*/
class MMM_IMPORT_EXPORT Motion : public AbstractMotion
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    Motion(const std::string& name);

	/*!
		Adds a snapshot to the motion data.
		The dimensions of the joint data is checked against the internal ndof value.
		The data is added without checking the timestep order.
		\param md The data to add.
		\return True on success.
	*/
	bool addMotionFrame(MotionFramePtr md);
    bool removeMotionFrame(size_t frame);

	/*!
		The joint names can be specified here. This implicitly sets the number of DoFs.
	*/
	bool setJointOrder(const std::vector<std::string> &jointNames);

    //! Returns the name
	std::string getName();

	/*!
		Specifies a comment string.
		Overwrites current comment.
		Adds standard MotionEntry with name "comments".
	*/
    void setComment(const std::string &comment);
    void addComment(const std::string &comment);

    //! Returns comment strings combined to one std::string.
	std::string getComment();

    //! Returns the joint names.
    std::vector<std::string> getJointNames();

	/*!
		Returns the corresponding frame.
		If frame is out of bounds, an empty MotionFrame is returned.
	*/
	MotionFramePtr getMotionFrame(size_t frame);

	//! Returns the vector with all MotionFrame objects.
    std::vector<MotionFramePtr> getMotionFrames();

	/*!
		Returns the number of entries.
	*/
	virtual unsigned int getNumFrames();

	void setModel(ModelPtr model);
	void setModel(ModelPtr processedModel, ModelPtr originalModel);

    /*!
        Return the model.
        @param processedModel Indicates weather the processed or the original model should be returned (If no modelProcessor is specified, the originalModel is always returned).
        @return The model or ModelPtr() if no model is not specified.
    */
    ModelPtr getModel(bool processedModel = true);

    const std::string& getMotionFilePath();
    void setMotionFilePath(const std::string&filepath);
    //! returns the motion file name and extension
    const std::string& getMotionFileName();
    //! stores the motion file name and extension
    void setMotionFileName(const std::string&filename);


	void setModelProcessor(ModelProcessorPtr mp);


    /*!
        Return the ModelProcessor (if specified).
    */
    ModelProcessorPtr getModelProcessor();

	/*!
		Creates an XML string for this object.
	*/
	virtual std::string toXML();

	//! Prints the XML string to std::cout
	void print();

	//! Validates if a given joint name is present
	bool hasJoint( const std::string &name );

    //! Calculate Joint Velocities from Joint Values
    void calculateVelocities(int method = 0);

    //! Calculate Joint Accelerations from Joint Velocities
    void calculateAccelerations(int method = 0);

protected:

    std::vector<MotionFramePtr> motionFrames;
	std::vector<std::string> jointNames;
	std::string name;

    ModelPtr model; // the processed model
    ModelPtr originalModel;
    ModelProcessorPtr modelProcessor;
    std::string motionFilePath;
    std::string motionFileName; //the motion file name and extension


private:
    Eigen::MatrixXf getJointValuesAsMatrix();
    Eigen::MatrixXf getJointVelocitiesAsMatrix();
    Eigen::MatrixXf getJointAccelerationsAsMatrix();
    Eigen::MatrixXf calculateDifferentialQuotient(const Eigen::MatrixXf &inputMatrix, int method = 0);
};

typedef boost::shared_ptr<Motion> MotionPtr;
typedef std::vector<MotionPtr> MotionList;

}

#endif
