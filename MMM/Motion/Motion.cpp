#include "Motion.h"
#include "../rapidxml.hpp"
#include "../XMLTools.h"
#include <boost/filesystem.hpp>

using std::cout;
using std::endl;

namespace MMM
{

Motion::Motion (const std::string& name)
:name(name)
{
}

bool Motion::addMotionFrame(MotionFramePtr md)
{
	if (!md)
		return false;
	if (jointNames.size()>0 && md->joint.rows()>0)
		if (md->joint.rows() != jointNames.size())
		{
			MMM_ERROR << "Error: md.joint.rows()=" << md->joint.rows() << ", but jointNames.size()=" << jointNames.size() << endl;
			return false;
		}
	motionFrames.push_back(md);
    return true;
}

bool Motion::removeMotionFrame(size_t frame)
{
    if(frame >= motionFrames.size())
        return false;
    motionFrames.erase(motionFrames.begin()+frame);
    return true;
}

bool Motion::setJointOrder(const std::vector<std::string> &jointNames)
{
	if (jointNames.size()==0)
		return false;
	this->jointNames = jointNames;
	return true;
}

void Motion::setComment(const std::string &comment)
{
	CommentEntryPtr c(new CommentEntry());
	c->comments.push_back(comment);
	addEntry("comments",c);
}


void Motion::addComment(const std::string &comment)
{
    if (!hasEntry("comments"))
        setComment(comment);
    else
    {
        MotionEntryPtr c1 = getEntry("comments");
        CommentEntryPtr c = boost::dynamic_pointer_cast<CommentEntry>(c1);
        if (c)
            c->comments.push_back(comment);
    }
}

void Motion::setModel(ModelPtr model)
{
	this->model = model;
	this->originalModel = model;
}

void Motion::setModel(ModelPtr processedModel, ModelPtr originalModel)
{
	this->model = processedModel;
    this->originalModel = originalModel;
}

ModelPtr Motion::getModel(bool processedModel)
{
    if(processedModel)
        return model;
    else
        return originalModel;
}

const std::string &Motion::getMotionFilePath()
{
    return motionFilePath;
}

const std::string &Motion::getMotionFileName()
{
    return motionFileName;
}

void Motion::setMotionFilePath(const std::string &filepath)
{
    this->motionFilePath = filepath;
}

void Motion::setMotionFileName(const std::string &filename)
{
    this->motionFileName = filename;
}

void Motion::setModelProcessor(ModelProcessorPtr mp)
{
    this->modelProcessor = mp;
}

ModelProcessorPtr Motion::getModelProcessor()
{
    return modelProcessor;
}

std::string Motion::toXML()
{
	std::string tab1 = "\t";
	std::string tab2 = "\t\t";
	std::string tab3 = "\t\t\t";
	std::stringstream res;
	//res << "<? xml version='1.0' ?>" << endl;

    res << tab1 << "<Motion name='" << name << "'>" << endl;
	std::map<std::string, MotionEntryPtr>::iterator i = motionEntries.begin();
	while (i != motionEntries.end())
	{
		res << i->second->toXML();
		i++;
	}
	if (originalModel && !originalModel->getFilename().empty())
	{
		std::string modelFile = originalModel->getFilename();
		if (!motionFilePath.empty())
		{
			// make relative path
            MMM::XML::makeRelativePath(motionFilePath, modelFile);
			//modelFile = MMM::XML::make_relative(motionFilePath, modelFile);
		}

		res << tab2 << "<Model>" << endl;
		res << tab3 << "<File>" << modelFile << "</File>" << endl;
		res << tab2 << "</Model>" << endl;
	}
	if (modelProcessor)
	{
		res << modelProcessor->toXML(2);
	}

	if (jointNames.size()>0)
	{
		res << tab2 << "<JointOrder>" << endl;
		for (size_t i=0;i<jointNames.size();i++)
		{
			res << tab3 << "<Joint name='" << jointNames[i] << "'/>" << endl;
		}
		res << tab2 << "</JointOrder>" << endl;
	}
	res << tab2 << "<MotionFrames>" << endl;		
	for (size_t i=0;i<motionFrames.size();i++)
	{
		res << motionFrames[i]->toXML(); 
	}
	res << tab2 << "</MotionFrames>" << endl;
	res << tab1 << "</Motion>" << endl;
	return res.str();
}

void  Motion::print()
{
	std::string s = toXML();
	cout << s;
}

std::vector<MotionFramePtr> Motion::getMotionFrames()
{
	return motionFrames;
}

MotionFramePtr Motion::getMotionFrame(size_t frame)
{
	if (frame>=motionFrames.size())
	{
		MMM_ERROR << "OutOfBounds error in getMotionFrame:" << frame << endl;
		return MotionFramePtr();
	}
	return motionFrames[frame];
}

std::vector<std::string> Motion::getJointNames()
{
	return jointNames;
}

unsigned int Motion::getNumFrames()
{
	return (unsigned int)motionFrames.size();
}

std::string Motion::getComment() 
{
	if (!hasEntry("comments"))
		return std::string();
	MotionEntryPtr e = getEntry("comments");
	CommentEntryPtr r = boost::dynamic_pointer_cast<CommentEntry>(e);
	if (!r)
		return std::string();
	return r->getCommentString();
}

std::string Motion::getName() {
    return name;
}


bool Motion::hasJoint(const std::string& name)
{
	std::string lc = name;
	XML::toLowerCase(lc);
	for(int i=0; i<(int)jointNames.size(); i++)
	{
		if(jointNames[i]==name)
		{
			return true;
		}
	}
	return false;
}

void Motion::calculateVelocities(int method)
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    Eigen::MatrixXf jointValues = getJointValuesAsMatrix();
    Eigen::MatrixXf jointVelocities = calculateDifferentialQuotient(jointValues,method);

    for (int i=0; i < frames.size(); i++)
    {
        frames[i]->joint_vel = jointVelocities.row(i);
    }

    return;
}

void Motion::calculateAccelerations(int method)
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    Eigen::MatrixXf jointVelocities = getJointVelocitiesAsMatrix();
    Eigen::MatrixXf jointAcclerations = calculateDifferentialQuotient(jointVelocities,method);

    for (int i=0; i < frames.size(); i++)
    {
        frames[i]->joint_acc = jointAcclerations.row(i);
    }

    return;
}


Eigen::MatrixXf Motion::getJointValuesAsMatrix()
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    int nFrames = frames.size();
    int nDof = frames[0]->joint.rows();

    Eigen::MatrixXf jointValues = Eigen::MatrixXf::Zero(nFrames, nDof);

    for (int i=0; i < nFrames; i++)
    {
        jointValues.row(i) = frames[i]->joint;
    }

    return jointValues;
}


Eigen::MatrixXf Motion::getJointVelocitiesAsMatrix()
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    int nFrames = frames.size();
    int nDof = frames[0]->joint.rows();

    Eigen::MatrixXf jointVelocities = Eigen::MatrixXf::Zero(nFrames,nDof);

    for (int i=0; i < nFrames; i++)
    {
        jointVelocities.row(i) = frames[i]->joint_vel;
    }

    return jointVelocities;
}
Eigen::MatrixXf Motion::getJointAccelerationsAsMatrix()
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    int nFrames = frames.size();
    int nDof = frames[0]->joint.rows();

    Eigen::MatrixXf jointAccelerations = Eigen::MatrixXf::Zero(nFrames,nDof);

    for (int i=0; i < nFrames; i++)
    {
        jointAccelerations.row(i) = frames[i]->joint_acc;
    }

    return jointAccelerations;
}




Eigen::MatrixXf Motion::calculateDifferentialQuotient(const Eigen::MatrixXf &inputMatrix, int method)
{
    Eigen::MatrixXf outputMatrix = Eigen::MatrixXf(inputMatrix.rows(),inputMatrix.cols());

    if (method == 0) // centralized
    {
        Eigen::VectorXf x1 = Eigen::VectorXf(inputMatrix.cols());
        Eigen::VectorXf x2 = Eigen::VectorXf(inputMatrix.cols());


        //iterate over all frames
        for (int i=0; i< inputMatrix.rows(); i++)
        {
            if (i==0)
                x1 = inputMatrix.row(0);
            else
                x1 = inputMatrix.row(i-1);

            if(i==inputMatrix.rows()-1)
                x2 = inputMatrix.row(inputMatrix.rows()-1);
            else
                x2 = inputMatrix.row(i+1);

            outputMatrix.row(i) = (x2-x1)/2;
        }

    }

    return outputMatrix;
}
}//namespace


