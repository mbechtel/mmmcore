/** 
\page mmmtools The MMM Tools

  The MMMtools project contains tools for visualization, reproduction, and recognition.
  
  The repository can be found here: https://i61wiki.itec.uka.de/git/mmmtools.git
  
\section mmmsimoxtools MMMSimoxTools

This library provides MMM related tools which allow to use the functionality of the C++ robotics toolbox Simox,
which can be found here:
https://sourceforge.net/projects/simox/

Documentation is available here:
http://sourceforge.net/apps/mediawiki/simox

  
\section mmmviewer MMMViewer

  The MMMViewer can be used to visualize \ref models, Motions, Marker Trajectories and Robots.

  The following commandline options are available:

   <!-- - &ndash;&nbsp;&ndash;&nbsp;model <mymodel.xml> <br>
   The model to display -->
   - &ndash;&nbsp;&ndash;&nbsp;motion <motiondata.xml> <br>
   The motion data in MMM specification. The file is additionally parsed for model files and modelprocessor configurations which are used to specify the model processor in order to setup the model.

    \image html MMMViewer.png "The MMMViewer window"


\section mmmconverter MMMConverter

The MMMConverter can be used to apply converters which are derived from MMM::Converter in order to convert motions from one model to another. Both conversions are supported (human motion -> MMM and MMM -> robot).
By default (when no command line options are given) an exemplary conversion of a c3d marekr motion to the Winter MMM model is performed.

The following commandline options are available:

 - &ndash;&nbsp;&ndash;&nbsp;inputModel <inModel.xml> <br>
   The input model file that should be used for conversion. (Ignored when Vicon data is converted in Vicon->MMM mode)
 
 - &ndash;&nbsp;&ndash;&nbsp;inputModelProcessor <processorName> <br>
   Optional tag that specifies the ModelProcessor that converts the input model before applying the converter. (Ignored when vicon data is converted in Vicon->MMM mode)
 
 - &ndash;&nbsp;&ndash;&nbsp;inputModelProcessorConfigFile <config.xml> <br>
   Optional config file for the input model porcessor. (Ignored when vicon data is converted in Vicon->MMM mode)
   
 - &ndash;&nbsp;&ndash;&nbsp;outputModel <outModel.xml> <br>
   The output model file that should be used for conversion. (Could be an MMM model for Vicon->MMM converters or a Robot model for MMM->Robot converters)
 
 - &ndash;&nbsp;&ndash;&nbsp;outputModelProcessor <processorName> <br>
   Optional tag that specifies the ModelProcessor that converts the output model before applying the converter. 
 
 - &ndash;&nbsp;&ndash;&nbsp;outputModelProcessorConfigFile <config.xml> <br>
   Optional config file for the output model porcessor.
   
 - &ndash;&nbsp;&ndash;&nbsp;inputDataVicon <datafile.c3d> <br>
   In case vicon marker data is converted (vicon->MMM) this tag specifies the input motion file.
   
 - &ndash;&nbsp;&ndash;&nbsp;inputDataMMM <mmmDataFile.xml> <br>
   In case mmm data is converted (MMM->Robot) this tag specifies the input mmm motion file.
   
 - &ndash;&nbsp;&ndash;&nbsp;outputFile <outputfile.xml> <br>
   The output file name. The resulting motion is stored here.
   
 - &ndash;&nbsp;&ndash;&nbsp;libPath <path/to/converter/libraries,/path/to/other/converter/libs/> <br>
   Here, converter libraries are searched. Comma separated list of directories.
   
 - &ndash;&nbsp;&ndash;&nbsp;converter <ConverterName> <br>
   Specifies which converter is used. The string is compared with all ConverterFactories that are present in the defined library directories.
 
 - &ndash;&nbsp;&ndash;&nbsp;converterConfigFile <ConverterConfig.xml> <br>
   The configuration file of the converter.


\section  mmmconvertergui MMMConverterGUI

The MMMconverterGui offers a graphical user interface for marker-based Converters. These class of converters are usually used for an initial mapping between human motion capture data 
(e.g. vicon marker based) and an MMM reference model (e..g Winter).  
The converter GUI offers the possibility to interact with the converter library (see \ref mmmviconconverter). Several options are available:


 - &ndash;&nbsp;&ndash;&nbsp;inputDataVicon <datafile.c3d> <br>
   This specifies the input marker-based motion. (Standard: PATH_TO_MMM_TOOLS+'/data/WalkForwardSLSL03.c3d')
   
 - &ndash;&nbsp;&ndash;&nbsp;outputModel <outModel.xml> <br>
   The target model file that should be used for conversion. (Standard: PATH_TO_MMM_TOOLS+'/data/Model/Winter/Winter.xml')
 
 - &ndash;&nbsp;&ndash;&nbsp;outputModelProcessor <processorName> <br>
   This string identifies the \ref modelprocessor "model processor" which should be used. (Standard: 'Winter')
 
 - &ndash;&nbsp;&ndash;&nbsp;outputModelProcessorConfigFile <config.xml> <br>
   The configuration file of the model processor. (Standard: PATH_TO_MMM_TOOLS+'/data/ModelProcessor_Winter_1.70.xml')
      
 - &ndash;&nbsp;&ndash;&nbsp;outputFile <outputfile.xml> <br>
   The output file name. The resulting motion is stored here.
   
 - &ndash;&nbsp;&ndash;&nbsp;libPath <path/to/converter/libraries,/path/to/other/converter/libs/> <br>
   Here, converter libraries are searched. Comma separated list of directories.
   
 - &ndash;&nbsp;&ndash;&nbsp;converter <ConverterName> <br>
  Specifies which \ref converters "converter" is used. The string is compared with all ConverterFactories that are present in the defined library directories. (Standard: 'ConverterVicon2MMM')
 
 - &ndash;&nbsp;&ndash;&nbsp;converterConfigFile <ConverterConfig.xml> <br>
   The configuration file of the converter. (Standard: PATH_TO_MMM_TOOLS+'/data/ConverterVicon2MMM_WinterConfig.xml')


The GUI offers the possibility to visualize the model, input and output markers and to inspect the progress of the convertion process. The converter can be intiialized and 
either a stepwise conversion (for debugging purposes) or a complete transformation of the motion can be performed. Furthermore, the results can be inspected, stored to the MMM::Motion format 
and the resulting motion can be saved as a stream of PNG images for further processing.

  \image html MMMConverterGUI_small.png "The MMMConvertrGUI window"
 
\section mmmviconconverter The ConverterVicon2MMM Converter

This library provides an exemplary implementaion of an MMM::Converter that maps vicon marker data to a \ref models "reference MMM Model".
It can be used within the MMMConverter framework. The usage is exemplarily explained below:
\code
	// get a converter factory from name
	std::string converterName("ConverterVicon2MMM");
	MMM::ConverterFactoryPtr converterFactory = MMM::ConverterFactory::fromName(converterName, NULL);
	if (!converterFactory)
	{
		cout << "Could not create converter factory of type " << converterName << endl;
		cout << "Setting up Converter... Failed..." << endl;
		return;
	}
	
	// Create a converter instance
	MMM::ConverterPtr c = converterFactory->createConverter();
	if (!c)
	{
		MMM_ERROR << "Could not build converter..." << endl;
		return;
	}
	
	// We know it is a marker based converter
	converter = boost::dynamic_pointer_cast<MMM::MarkerBasedConverter>(c);
	if (!converter)
	{
		MMM_ERROR << "Could not cast converter to MarkerBasedConverter..." << endl;
		return;
	}

	// load configuration file
	if (!converter->setupFile(converterFile))
	{
		cout << "Error while configuring converter '" << converterName << "' from file " << converterFile << endl;
		cout << "Setting up Converter... Failed..." << endl;
	}

	// get the input-to-output marker mapping
	std::map<std::string, std::string> markerMapping = converter->getMarkerMapping();
\endcode

An exemplary ConverterVicon2MMM config file may look like this
\code
<?xml version='1.0' encoding='UTF-8'?>
<ConverterConfig type='ConverterVicon2MMM'>
  <Model name='Winter'>
	<MarkerMapping>    
		<Mapping c3d="C7"   mmm="C7_EMPTY"/>  
		<Mapping c3d="L3"   mmm="L3_EMPTY"/>  
		<Mapping c3d="CLAV" mmm="CLAV_EMPTY"/>
		<Mapping c3d="RBAK" mmm="RBAK_EMPTY"/>
		<Mapping c3d="T10"  mmm="T10_EMPTY"/>
		<Mapping c3d="STRN" mmm="STRN_EMPTY"/>
		<Mapping c3d="RBHD" mmm="RBHD_EMPTY"/>
		<Mapping c3d="LBHD" mmm="LBHD_EMPTY"/>
		<Mapping c3d="RASI" mmm="RASI_EMPTY"/>
		<Mapping c3d="LAEL" mmm="LAEL_EMPTY"/>
		<Mapping c3d="LUPA" mmm="LUPA_EMPTY"/>
		<Mapping c3d="LASI" mmm="LASI_EMPTY"/>
		<Mapping c3d="LHIP" mmm="LHIP_EMPTY"/>
		<Mapping c3d="LPSI" mmm="LPSI_EMPTY"/> 
		<Mapping c3d="LKNE" mmm="LKNE_EMPTY"/>
		<Mapping c3d="LSHO" mmm="LSHO_EMPTY"/>
		<Mapping c3d="RPSI" mmm="RPSI_EMPTY"/> 
		<Mapping c3d="LFRA" mmm="LFRA_EMPTY"/>
		<Mapping c3d="LWPS" mmm="LWPS_EMPTY"/>
		<Mapping c3d="LWTS" mmm="LWTS_EMPTY"/>
		<Mapping c3d="LANK" mmm="LANK_EMPTY"/>
		<Mapping c3d="LHEE" mmm="LHEE_EMPTY"/>
		<Mapping c3d="LMT1" mmm="LMT1_EMPTY"/>
		<Mapping c3d="LMT5" mmm="LMT5_EMPTY"/>
		<Mapping c3d="LTOE" mmm="LTOE_EMPTY"/>
		<Mapping c3d="RAEL" mmm="RAEL_EMPTY"/>
		<Mapping c3d="RUPA" mmm="RUPA_EMPTY"/>   
		<Mapping c3d="RSHO" mmm="RSHO_EMPTY"/>
		<Mapping c3d="RHIP" mmm="RHIP_EMPTY"/>
		<Mapping c3d="RKNE" mmm="RKNE_EMPTY"/>           
		<Mapping c3d="RWPS" mmm="RWPS_EMPTY"/>
		<Mapping c3d="RWTS" mmm="RWTS_EMPTY"/>         
		<Mapping c3d="RANK" mmm="RANK_EMPTY"/>
		<Mapping c3d="RHEE" mmm="RHEE_EMPTY"/>
		<Mapping c3d="RMT1" mmm="RMT1_EMPTY"/>
		<Mapping c3d="RMT5" mmm="RMT5_EMPTY"/>
		<Mapping c3d="RTOE" mmm="RTOE_EMPTY"/>                            		
	</MarkerMapping> 	
	
	<JointSet>

		<!-- Torso -->
		<Joint name='BTx_joint'/>
		<Joint name='BTy_joint'/>
		<Joint name='BTz_joint'/>
		
		<!-- Left Foot -->
		<Joint name='LAx_joint'/>
		<Joint name='LAy_joint'/>
		<Joint name='LAz_joint'/>
		
		<!-- Left Elbow -->
		<Joint name='LEx_joint'/>
		<Joint name='LEy_joint'/>
		
		<!-- Left hip -->
		<Joint name='LHx_joint'/>
		<Joint name='LHy_joint'/>
		<Joint name='LHz_joint'/>
		
		<!-- Left Knee -->
		<Joint name='LKx_joint'/>
		
		<!-- Left Shoulder inner -->
		<Joint name='LSCx_joint'/>
		<Joint name='LSCy_joint'/>
		<Joint name='LSCz_joint'/>
		
		<!-- Left Shoulder outer -->
		<Joint name='LSx_joint'/>
		<Joint name='LSy_joint'/>
		<Joint name='LSz_joint'/>
		
		<!-- Left Wrist -->
		<Joint name='LWx_joint'/>
		<Joint name='LWz_joint'/>
		
		<!-- Right Foot -->
		<Joint name='RAx_joint'/>
		<Joint name='RAy_joint'/>
		<Joint name='RAz_joint'/>
		
		<!-- Right Elbow -->
		<Joint name='REx_joint'/>
		<Joint name='REy_joint'/>
		
		<!-- Right Hip -->
		<Joint name='RHx_joint'/>
		<Joint name='RHy_joint'/>
		<Joint name='RHz_joint'/>
		
		<!-- Right Knee -->
		<Joint name='RKx_joint'/>
		
		<!-- Right Shoulder inner -->
		<Joint name='RSCx_joint'/>
		<Joint name='RSCy_joint'/>
		<Joint name='RSCz_joint'/>
		
		<!-- Right Shoulder outer -->		
		<Joint name='RSx_joint'/>
		<Joint name='RSy_joint'/>
		<Joint name='RSz_joint'/>
		
		<!-- Right Wrist -->
		<Joint name='RWx_joint'/>
		<Joint name='RWz_joint'/>
	</JointSet>	
  </Model>
  <IK>
    <InitialIKParams ikSteps='50' checkImprovement='false' minChange='0' stepSize='0.8'/>
    <StepIKParams ikSteps='10' checkImprovement='true' minChange='0.01' stepSize='0.8'/>
  </IK>
</ConverterConfig> 		
\endcode


The converter can be used as follows:
\code

// 1. load an (output) model
MMM::ModelReaderXMLPtr r(new MMM::ModelReaderXML());
MMM::ModelPtr mmmOrigModel = r->loadModel(modelFile);

// 2. process model
MMM::ModelPtr mmmModel = modelProcessor->convertModel(mmmOrigModel);

// 3. load marker based vicon motion
MMM::MotionReaderC3DPtr c(new MMM::MotionReaderC3D());
MMM::MarkerMotionPtr markerMotion = c->loadC3D(viconFile);

//  4. setup with 
// empty input model (vicon marker data does not rely on an input model)
// the marker based motion
// and the ouput model
converter->setup(MMM::ModelPtr(), markerMotion, mmmModel);

// 5. intialize converter
MMM::AbstractMotionPtr m = converter->initializeStepwiseConvertion();
MMM::MotionPtr resultModelMotion = boost::dynamic_pointer_cast<MMM::Motion>(m);

// 6. convert one step
bool res = converter->convertMotionStep(resultModelMotion);

// 5/6. or convert complete motion
MMM::AbstractMotionPtr m = converter->convertMotion();
MMM::MotionPtr resultModelMotion = boost::dynamic_pointer_cast<MMM::Motion>(m);

// 7. store result to file
std::filename("outputmotion.xml");
std::string content = resultModelMotion->toXML();
MMM::XML::saveXML(filename, content);
\endcode

*/
